const express = require('express')
const router = express.Router()

const { createPostComment, getAllPostComment, getOnePostComment, updatePostComment, deletPostComment } = require('../controllers/post_comment');

router.post('/', createPostComment)
router.get('/', getAllPostComment)
router.get('/:id', getOnePostComment)
router.put('/:id', updatePostComment)
router.delete('/:id', deletPostComment)


module.exports = router;