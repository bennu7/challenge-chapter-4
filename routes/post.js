const express = require('express');
const router = express.Router()
const { createPost, getAllPosts, getOnePost, updatePost, deleteUser, deletePost } = require('../controllers/post')

router.post('/', createPost)
router.get('/', getAllPosts)
router.get('/:id', getOnePost)
router.put('/:id', updatePost)
router.delete('/:id', deletePost)

module.exports = router;
