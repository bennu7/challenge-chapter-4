const express = require('express');
const router = express.Router();
const userRouter = require('./user')
const postRouter = require('./post')
const postCommentRouter = require('./post_comment')
const postLikeRouter = require('./post_like')

router.get('/', (req, res) => {
    res.status(200).json({
        status: 'success',
        message: 'Selamat datang di API instagram',
        data: null
    })
})

router.use('/users', userRouter)
router.use('/posts', postRouter)
router.use('/post_comments', postCommentRouter)
router.use('/post_likes', postLikeRouter)

module.exports = router;