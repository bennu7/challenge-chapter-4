const express = require('express')
const router = express.Router();

const { createPostLike, getAllPostLike, getOnePostLike, updatePostLike, deletePostLike } = require('../controllers/post_like')

router.post('/', createPostLike)
router.get('/', getAllPostLike)
router.get('/:id', getOnePostLike)
router.put('/:id', updatePostLike)
router.delete('/:id', deletePostLike)



module.exports = router