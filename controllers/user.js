const { User } = require('../models');

createUser = async (req, res) => {
    try {

        let { name, username, email, password } = req.body;

        let newUser = await User.create({
            name, username, email, password
        });

        // jika berhasil 
        res.status(201).json({
            status: 'success',
            message: 'data user instagram baru telah di buat',
            data: newUser
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        })
    }
}

getAllUser = async (req, res) => {
    try {
        let users = await User.findAll({ include: 'post' });

        res.status(200).json({
            status: 'success',
            message: 'sukses GET semua data user',
            data: users
        })

    } catch (err) {
        console.log(err)
        res.status(500).json({
            status: 'error',
            errors: err
        })
    }
}

getOneUser = async (req, res) => {
    try {
        const user_id = req.params.id;

        let user = await User.findOne({
            where: {
                id: user_id
            },
            include: ['post', 'post_likes', 'post_comments']
        })

        if (!user) {
            res.status(404).json({
                status: 'error',
                message: `tidak ditemukan dengan id : ${user_id}`,
                data: null
            })
        }

        res.status(200).json({
            status: 'success',
            message: 'sukses GET detail data user',
            data: user
        })
    } catch (err) {
        console.log(err)
        res.status(500).json({
            status: 'error',
            errors: err
        })
    }
}

updateUser = async (req, res) => {
    try {
        let user_id = req.params.id;
        const { name, username, email, password } = req.body;

        // fungsi query ini harus dipahami
        let query = {
            where: {
                id: user_id
            }
        }

        let updated = await User.update({
            name,
            username,
            email,
            password
        }, query);

        res.status(200).json({
            status: 'success',
            message: 'update user telah berhasil',
            data: updated
        })

    } catch (err) {
        console.log(err)
        res.status(500).json({
            status: 'error',
            errors: err
        })

    }
}

deleteUser = async (req, res) => {
    try {
        let user_id = req.params.id;

        let deleted = await User.destroy({
            where: {
                id: user_id
            }
        })

        res.status(200).json({
            status: 'success',
            message: 'data user sukses diupdate',
            data: deleted
        })
    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        })
    }
}

module.exports = {
    createUser,
    getAllUser,
    getOneUser,
    updateUser,
    deleteUser
}